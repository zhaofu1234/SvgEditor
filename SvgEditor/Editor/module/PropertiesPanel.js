/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.module.PropertiesPanel', {
	extend: 'SvgEditor.property.Grid',
	locale: null,
	stripeRows: true,
	collapsible: true,
	collapsed: true,
	groupField: 'group',
	chilcksToEdit: 1,
	canvas: null,
	treeStore: null,
	initComponent: function() {
		var me = this;
		me.baseTitle = me.locale.propertiesPanel.title;
		me.tooltipRenderer = function(value, p, record) {
			p.tdAttr = 'title="' + record.data.gridProperties.tooltip + '"';
			return value;
		};
		me.callParent();
		for (var i = 0; i < 2; i++) {
			Ext.apply(me.columns[i], {text: me.locale.propertiesPanel.columns[i]});
		}
	},
	initAttributes: function(propertiesPanel, attributes) {
		if (attributes) {
			propertiesPanel.modelType = 'BPMNDiagram';
			propertiesPanel.attributes = attributes;
		}
		propertiesPanel.setAttributes(propertiesPanel, propertiesPanel.modelType);
	},
	setAttributes: function(propertiesPanel) {
		var modelType;
		if (propertiesPanel.canvas.selected.length == 1) {
			propertiesPanel.currentModel = propertiesPanel.canvas.selected[0];
			modelType = propertiesPanel.canvas.getData(propertiesPanel.canvas, 'modelType');
		} else {
			propertiesPanel.currentModel = null;
			modelType = propertiesPanel.modelType;
		}
		var node = propertiesPanel.treeStore.getNodeById(modelType), data, attributes = [];
		if (node) {
			data = node.data;
			var properties = data.properties;
			Ext.each(properties, function(property) {
				var attrs = propertiesPanel.attributes[property];
				Ext.each(attrs, function(attr) {
					if (attr.visible !== false) {
						if (!attr.gridProperties) {
							attr.gridProperties = {
								editor: 'combobox',
								propId: attr.id,
								tooltip: attr.description
							};
						}
						attributes.push(attr);
					}
				});
			});
		} else {
			data = {text: propertiesPanel.locale.propertiesPanel.unKnowModel};
		}
		propertiesPanel.setTitle(propertiesPanel.baseTitle + '-' + data.text);
		propertiesPanel.setSource(attributes);
	}
});