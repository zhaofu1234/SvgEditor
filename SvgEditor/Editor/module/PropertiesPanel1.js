/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.module.PropertiesPanel', {
	extend: 'Ext.grid.property.Grid',
	locale: null,
	stripeRows: true,
	collapsible: true,
//	collapsed: true,
	features: [{
			groupHeaderTpl: '{name}',
			ftype: 'grouping'
		}],
//	plugins: [{ptype: 'cellediting', clicksToEdit: 1}],
	initComponent: function() {
		var me = this;
		me.baseTitle = me.locale.propertiesPanel.title;
		me.initColumns(me);
		me.callParent();
//		var rec = new Ext.grid.property.Property({
//			name: 'birthday',
//			value: Ext.Date.parse('17/06/1962', 'd/m/Y'),
//			type: '111',
//			fields: [{name: 'id', type: 'string'},
//				{name: 'name', type: 'string'},
//				{name: 'type', type: 'string'},
//				{name: 'value', type: 'string'}]
//		});
//		me.getStore().addSorted(rec);
//		me.getStore().loadData([rec]);
	},
	initColumns: function(me) {
		me.store = Ext.create('Ext.data.Store', {
			groupField: 'type',
			fields: [
				{name: 'id', type: 'string'},
				{name: 'name', type: 'string'},
				{name: 'type', type: 'string'},
				{name: 'value', type: 'string'}
			],
			proxy: {
				type: 'ajax',
				url: '',
				reader: {
					type: 'json'
				}
			}
		});
		me.columns = [
			{
				text: me.locale.propertiesPanel.columns[0],
				dataIndex: 'id', flex: 1
			},
			{
				text: me.locale.propertiesPanel.columns[1],
				dataIndex: 'value', flex: 2,
				//editable: true, editor: 'textfield'
			}
		];
//		me.source = {
//			"(name)": "My Object",
//			"Created": Ext.Date.parse('10/15/2006', 'm/d/Y'),
//			"Available": false,
//			"Version": 0.01,
//			"Description": "A test object"
//		};
//		me.source = [
//			{
//				name: '属性1',
//				value: '1',
//				gridProperties: {
//					editor: 'combobox',
//					tooltip: '11111'
//				}
//			}
//		];
		me.customEditors = {
			"name": Ext.create('Ext.form.field.ComboBox')
		};
	},
	initAttributes: function(propertiesPanel, attributes) {
		if (attributes) {
			propertiesPanel.modelType = propertiesPanel.locale.canvasPanel.graph['process'].modelType;
			propertiesPanel.attributes = attributes;
		}
		propertiesPanel.setAttributes(propertiesPanel, propertiesPanel.modelType);
	},
	setAttributes: function(propertiesPanel, modelType) {
		if (!modelType) {
			modelType = propertiesPanel.modelType
		}
		propertiesPanel.setTitle(propertiesPanel.baseTitle + '-' + modelType.name || modelType.id);
		var attributes = propertiesPanel.attributes[modelType.id];
		if (!attributes) {
			attributes = [];
		}
		propertiesPanel.store.loadData(attributes);
	}
});