/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.module.ModelTree', {
	extend: 'Ext.tree.Panel',
	rootVisible: false,
	enableDragDrop: true,
	locale: null,
	propertiesPanel: null,
	util: null,
	viewConfig: {
		plugins: {
			ptype: 'treeviewdragdrop',
			ddGroup: 'model-to-canvas',
			enableDrop: false
		}
	},
	collapsible: true,
	initComponent: function() {
		var me = this;
		me.title = me.locale.modelTree.title;
		me.initModel(me);
		me.initTbar(me);
		me.callParent();
	},
	initModel: function(me) {
		me.store = Ext.create('Ext.data.TreeStore', {
			fields: [
				{name: 'id', type: 'string'},
				{name: 'text', type: 'string'},
				{name: 'icon', type: 'string'},
				{name: 'leaf', type: 'bool'},
				{name: 'type', type: 'string'},
				{name: 'view', type: 'string'},
				{name: 'attrs', type: 'object'},
				{name: 'data', type: 'object'},
				{name: 'properties', type: 'object'},
				{name: 'roles', type: 'object'}
			],
			proxy: {
				type: 'ajax',
				url: 'model/Model_' + me.locale.locale + '.json',
				reader: {
					type: 'json'
//					root: 'model'
				}
			},
			listeners: {
				load: {
					fn: function() {
						var attributesJSON = me.util.ajax('Get', 'Properties_' + me.locale.locale + '.json'),
								attributes = Ext.decode(attributesJSON);
						me.propertiesPanel.initAttributes(me.propertiesPanel, attributes);
					}
				}
			}
		});
	},
	initTbar: function(me) {
		me.filterField = Ext.create('Ext.form.field.Text', {
			xtype: 'textfield',
			fieldLabel: me.locale.modelTree.tbar.filterField,
			labelWidth: 40
		});
		me.filterField.on('change', function() {
			me.filterBy(this.getValue(), 'text');
		});
		me.tbar = Ext.create('Ext.form.Panel', {
			layout: 'fit',
			frame: true,
			items: [me.filterField]
		});
	},
	filterBy: function(text, by) {
		this.clearFilter();
		var view = this.getView(),
				me = this,
				nodesAndParents = [];
		// Find the nodes which match the search term, expand them.
		// Then add them and their parents to nodesAndParents.
		this.getRootNode().cascadeBy(function(tree, view) {
			var currNode = this;

			if (currNode && currNode.data[by] && currNode.data[by].toString().toLowerCase().indexOf(text.toLowerCase()) > -1) {
				me.expandPath(currNode.getPath());
				while (currNode.parentNode) {
					nodesAndParents.push(currNode.id);
					currNode = currNode.parentNode;
				}
			}
		}, null, [me, view]);
		// Hide all of the nodes which aren't in nodesAndParents
		this.getRootNode().cascadeBy(function(tree, view) {
			var uiNode = view.getNodeByRecord(this);
			if (uiNode && !Ext.Array.contains(nodesAndParents, this.id)) {
				Ext.get(uiNode).setDisplayed('none');
			}
		}, null, [me, view]);
	},
	clearFilter: function() {
		var view = this.getView();
		this.getRootNode().cascadeBy(function(tree, view) {
			var uiNode = view.getNodeByRecord(this);
			if (uiNode) {
				Ext.get(uiNode).setDisplayed('table-row');
			}
		}, null, [this, view]);
	},
	afterFirstLayout: function() {
		var me = this;
		me.callParent(arguments);
		var canvas = me.up().canvas, body = canvas.body;
		me.cavnasDropTarget = new Ext.dd.DropTarget(body, {
			ddGroup: 'model-to-canvas',
			notifyEnter: function(ddSource, e, data) {
				//Add some flare to invite drop.
				body.stopAnimation();
				body.highlight();
			},
			notifyDrop: function(ddSource, e, data) {
				var nodeConfig = data.records[0].data;
				var xy = e.getXY();
				xy[0] += canvas.body.dom.scrollLeft;
				xy[1] += canvas.body.dom.scrollTop;
				canvas.addModel(canvas, nodeConfig, xy[0], xy[1], true);
				return true;
			}
		});
	}
});