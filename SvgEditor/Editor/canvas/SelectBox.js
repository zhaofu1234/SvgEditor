/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.canvas.SelectBox', {
	selecteBox: null,
	lineX: null,
	lineY: null,
	initLineXY: function(canvas) {
		var canvas = canvas || this, lineXConfig = {
			modelId: 'lineX',
			type: 'path',
			attrs: {
				path: [['M', 0, 0], ['L', 0, canvas.paper.height]],
				fill: 'none', stroke: 'green', 'stroke-width': 1,
				'stroke-linecap': 'round',
				'stroke-linejoin': 'round',
				'stroke-dasharray': "-"
			}
		}, lineYConfig = {
			modelId: 'lineX',
			type: 'path',
			attrs: {
				path: [['M', 0, 0], ['L', canvas.paper.width, 0]],
				fill: 'none', stroke: 'green', 'stroke-width': 1,
				'stroke-linecap': 'round',
				'stroke-linejoin': 'round',
				'stroke-dasharray': "-"
			}
		};
		canvas.lineX = canvas.mixins[lineXConfig.type].init(canvas, lineXConfig, true).hide();
		canvas.lineY = canvas.mixins[lineXConfig.type].init(canvas, lineYConfig, true).hide();
	},
	initSelectBox: function(canvas) {
		var selectBoxConfig = {
			modelId: 'selectBox',
			type: 'rect',
			attrs: {
				fill: 'none', stroke: 'blue', 'stroke-width': 1,
				'stroke-linecap': 'round',
				'stroke-linejoin': 'round',
				'stroke-dasharray': "-"
			}
		};
		canvas.selectBox = canvas.createModel(canvas, selectBoxConfig).hide();
	},
	setAnchor: function(canvas, selectBox, xy) {
		selectBox.anchorX = xy[0], selectBox.anchorY = xy[1];
		selectBox.attr({x: xy[0], y: xy[1], width: 0, height: 0}).show().toFront();
		selectBox.view = true;
	},
	changeSelectBox: function(canvas, selectBox, xy) {
		var x, y, width, height;
		if (selectBox.anchorX < xy[0]) {
			x = selectBox.anchorX;
			width = xy[0] - x;
		} else {
			x = xy[0];
			width = selectBox.anchorX - x;
		}
		if (selectBox.anchorY < xy[1]) {
			y = selectBox.anchorY;
			height = xy[1] - y;
		} else {
			y = xy[1];
			height = selectBox.anchorY - y;
		}
		selectBox.attr({
			x: x - canvas.origin.offsetX, y: y - canvas.origin.offsetY,
			width: width, height: height
		});
	},
	selectModel: function(canvas, model) {
		var selectBox = canvas.selectBox, bbox, propertiesPanel = canvas.propertiesPanel,
				minX = canvas.getWidth(), minY = canvas.getHeight(), maxX = 0, maxY = 0;
		canvas.focus();
		canvas.selected = [];
		if (propertiesPanel) {
			propertiesPanel.initAttributes(propertiesPanel);
		}
		canvas.viewAnchor(canvas, null);
		canvas.widget.hide();
		if (model) {
			canvas.selected = [model];
			if (model.type === 'path') {
				canvas.viewAnchor(canvas, model);
			} else {
				bbox = model.getBBox(), minX = bbox.x, minY = bbox.y, maxX = bbox.x2, maxY = bbox.y2;
				selectBox.attr({x: minX, y: minY, width: maxX - minX, height: maxY - minY}).show();
				selectBox.view = true;
				canvas.widget.show();
				canvas.widget.setXY([
					bbox.x2 + canvas.origin.offsetX + 2 - canvas.body.dom.scrollLeft,
					bbox.y + canvas.origin.offsetY - canvas.body.dom.scrollTop
				]);
			}
			if (propertiesPanel) {
				var modelData = model.data();
				propertiesPanel.setAttributes(propertiesPanel);
			}
		} else if (canvas.selected.length == 0) {
			var ms = canvas.modelSet = canvas.paper.setFinish();
			Ext.each(ms, function(model) {
				if (model.type !== 'path' && model.type !== 'text'
						&& canvas.selectBox.isPointInside(model.coreX, model.coreY)) {
					canvas.selected.push(model);
					bbox = model.getBBox();
					minX = minX < bbox.x ? minX : bbox.x;
					minY = minY < bbox.y ? minY : bbox.y;
					maxX = maxX > bbox.x2 ? maxX : bbox.x2;
					maxY = maxY > bbox.y2 ? maxY : bbox.y2;
				}
			});
			if (canvas.selected.length > 0) {
				canvas.selectBox.attr({x: minX, y: minY, width: maxX - minX, height: maxY - minY});
			} else {
				canvas.selectBox.hide();
				canvas.selectBox.view = false;
			}
			canvas.paper.setStart(ms);
		}
		selectBox.oldX = selectBox.attrs.x;
		selectBox.oldY = selectBox.attrs.y;
	},
	selectAll: function(canvas) {
		var ms = canvas.modelSet = canvas.paper.setFinish(), bbox,
				minX = canvas.getWidth(), minY = canvas.getHeight(), maxX = 0, maxY = 0;
		canvas.selected = [];
		Ext.each(ms, function(model) {
			if (model.type !== 'path' && model.type !== 'text') {
				canvas.selected.push(model);
				bbox = model.getBBox();
				minX = minX < bbox.x ? minX : bbox.x;
				minY = minY < bbox.y ? minY : bbox.y;
				maxX = maxX > bbox.x2 ? maxX : bbox.x2;
				maxY = maxY > bbox.y2 ? maxY : bbox.y2;
			}
		});
		canvas.selectBox.attr({x: minX, y: minY, width: maxX - minX, height: maxY - minY}).show();
		canvas.selectBox.view = true;
		canvas.paper.setStart(ms);
		canvas.selectBox.oldX = canvas.selectBox.attrs.x;
		canvas.selectBox.oldY = canvas.selectBox.attrs.y;
	},
	viewAnchor: function(me, model) {
		var anchor = me.anchor;
		anchor.startP.hide();
		anchor.startP.toFront();
		anchor.endP.hide();
		anchor.endP.toFront();
		Ext.each(anchor.knuckles, function(knuckle) {
			knuckle.hide();
			knuckle.toFront();
		});
		if (!model) {
			return;
		} else if (model.type === 'path') {
			var flow = model;
			me.selectBox.hide();
			me.selectBox.view = false;
			var ff = flow.flowFrom, ft = flow.flowTo,
					path = flow.attrs.path, len = path.length,
					start = flow.start, end = flow.end;
			if (ff) {
				anchor.startP.attr({
					cx: ff.coreX, cy: ff.coreY, fill: 'red', 'fill-opacity': 1
				}).show();
			} else {
				anchor.startP.attr({cx: start[0], cy: start[1], fill: 'white'}).show();
			}
			if (ft) {
				anchor.endP.attr({
					cx: ft.coreX, cy: ft.coreY, fill: 'red', 'fill-opacity': 1
				}).show();
			} else {
				anchor.endP.attr({cx: end[0], cy: end[1], fill: 'white'}).show();
			}
			if (len > 2) {
				var canvas = flow.canvas;
				canvas.modelSet = canvas.paper.setFinish();
				for (var i = 1; i < len - 1; i++) {
					var anchorConfig = {
						modelId: 'knuckle' + i,
						type: 'circle',
						attrs: {
							cx: path[i][1], cy: path[i][2],
							r: 3, fill: '#00ff33', 'fill-opacity': 1
						}
					};
					if (anchor.knuckles[i - 1]) {
						anchor.knuckles[i - 1].attr({cx: path[i][1], cy: path[i][2]}).show().toFront();
					} else {
						var knuckle = canvas.mixins.anchor.init(canvas, anchorConfig).toFront();
						anchor.knuckles.push(knuckle);
					}
					anchor.knuckles[i - 1].index = i;
				}
				canvas.paper.setStart(canvas.modelSet);
			}
		}
	}
});