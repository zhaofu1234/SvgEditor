/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.init.Editor', {
	readOnly: false,
	locale: {},
	constructor: function(config) {
		var me = this;
		if (config) {
			Ext.apply(me, config);
		}
		me.init(me);
	},
	init: function(me) {
		me.util = Ext.create('SvgEditor.util.Util');
		me.initLocale(me);
		Ext.QuickTips.init();
		me.ep = Ext.create('SvgEditor.module.EditorPanel', {readOnly: me.readOnly, util: me.util, locale: me.locale});
		var viewport = new Ext.container.Viewport({
			layout: 'fit',
			items: [me.ep]
		});
	},
	initLocale: function(me) {
		me.locale.locale = (navigator.language || navigator.browserLanguage).toLocaleLowerCase();
		var localeJSON = me.util.ajax('Get', 'locale/SvgEditor_' + me.locale.locale + '.json')
		Ext.apply(me.locale, Ext.decode(localeJSON));
	}
});
